package Steps;

import com.codeborne.selenide.Condition;
import io.cucumber.java.ru.Когда;
import io.cucumber.java.ru.Тогда;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class Scroll {
    @Когда("пользователь скроллит страницу")
    public void пользовательСкроллитСтраницу() {
        $(By.cssSelector(".AvatarListModal_AvatarList__3sUKr>div:nth-child(38)"));
    }

    @Тогда("страница проскроллена")
    public void страницаПроскроллена() {
        $(By.cssSelector(".AvatarListModal_AvatarList__3sUKr>div:nth-child(38)")).should(Condition.visible);
    }

}
