package Steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideDriver;
import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.ru.Когда;
import org.openqa.selenium.By;
import pages.BasicPage;

import static com.codeborne.selenide.Selenide.*;

public class Click {
    BasicPage basicPage = new BasicPage();


    @Когда("пользователь нажимает {string}")
    public void пользовательНажимаетКнопку(String arg0) {
        basicPage.click(arg0);
    }

    @Когда("пользователь нажмет кнопку {string}")
    public void пользовательНажметКнопку(String arg0) {
        basicPage.clickSpan(arg0);
    }

    @Когда("пользователь нажимает кноку удаления группы")
    public void пользовательНажимаетКнокуУдаленияГруппы() {
        $(By.cssSelector(".GroupList_CloseIcon__3eGfc")).click();
    }

    @Когда("пользователь нажимает на аватар")
    public void пользовательНажимаетНаАватар() {
        $(By.cssSelector(".Avatar.medium")).click();
    }

    @Когда("пользователь выбирает финский язык")
    public void пользовательВыбираетФинскийЯзык() {
        $(By.xpath("//*/span[text()='English']")).should(Condition.exist).click();
        $(By.xpath("//*[text()='Suomi']")).should(Condition.exist).click();
        
    }

    @Когда("пользователь выбирает английский язык")
    public void пользовательВыбираетАнглийскийЯзык() {
        $(By.xpath("//*/span[text()='Suomi']")).waitUntil(Condition.exist, 50000).click();
        $(By.xpath("//*[text()='English']")).should(Condition.exist).click();
        
    }


    @Когда("пользователь выбирает узбекский язык")
    public void пользовательВыбираетУзбекскийЯзык() {
        $(By.xpath("//*/span[text()='English']")).should(Condition.exist).click();
        $(By.xpath("/html/body/div[5]/div[3]/ul/li[3]")).should(Condition.exist).click();
    }

    @Когда("пользователь выбирает аватар")
    public void пользовательВыбираетАватар() {
        $(By.cssSelector(".AvatarListModal_AvatarList__3sUKr>div:nth-child(23)")).click();
    }
    @Когда("пользователь выбирает курс")
    public void пользовательВыбираетКурс() {
        $(By.xpath("//*[@id=\"scroll-container\"]/div/div[2]/div[1]/div/div/div/div[2]/div[6]/button/span")).click();
    }
    @Когда("пользователь выбирает тему")
    public void пользовательВыбираетТему() {
        SelenideElement Continue = $$(".Button-module_Button__3jZjx.Button-module_outlined__3uqaq.Button-module_large__2R5Ir.Button-module_green__1B0zN")
                .get(0);
        sleep(5000);
        Continue.click();
    }

    @Когда("пользователь нажимает на кнопку All")
    public void пользовательНажимаетНаКнопкуAll() {
        $(By.id("drop-down-field")).click();

       // sleep(5000000);
    }

    @Когда("пользователь нажимает крестик")
    public void пользовательНажимаетКрестик() {
        $(By.cssSelector(".MuiIconButton-label>svg>path")).click();
    }

    @Когда("пользователь нажимает Cancel")
    public void пользовательНажимаетCancel() {
        $$(".Button-module_Title__3vQ-G").get(3).click();
    }
}
