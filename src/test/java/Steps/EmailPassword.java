package Steps;

import io.cucumber.java.ru.Когда;
import pages.BasicPage;

public class EmailPassword {
    BasicPage basicPage = new BasicPage();
    @Когда("пользователь заполняет данные Email и Password")
    public void пользовательЗаполняетДанныеEmailИPassword() {
        basicPage.inputEmail();
        basicPage.inputPassword();
    }

    @Когда("пользователь заполняет данные неверного Email и верного Password")
    public void пользовательЗаполняетДанныеНеверногоEmailИВерногоPassword() {
        basicPage.inputInvalidEmail();
        basicPage.inputPassword();
    }

    @Когда("пользователь заполняет данные верного Email и неверного Password")
    public void пользовательЗаполняетДанныеВерногоEmailИНеверногоPassword() {
        basicPage.inputEmail();
        basicPage.inputInvalidPass();
    }

    @Когда("пользователь заполняет данные в поле Password")
    public void пользовательЗаполняетДанныеВПолеPassword() {
        basicPage.inputPassword();
    }
}
