package Steps;

import io.cucumber.java.ru.Когда;
import pages.BasicPage;

public class Login {
    BasicPage basicPage = new BasicPage();
    @Когда("пользователь нажимает кнопку {string}")
    public void пользовательНажимаетКнопку(String arg0) {
        basicPage.click(arg0);
    }
}
