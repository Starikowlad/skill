package Steps;

import com.codeborne.selenide.Condition;
import io.cucumber.java.ru.Когда;
import io.cucumber.java.ru.Тогда;
import pages.BasicPage;

public class HideShow {
    BasicPage basicPage = new BasicPage();
    @Когда("пользователь нажимает кнопку Show Password")
    public void пользовательНажимаетКнопкуShowPassword () {
        basicPage.ShowHide();
    }

    @Тогда("пароль отображается в поле Password")
    public void парольОтображаетсяВПолеPassword() {
        basicPage.showpass();
    }

    @Когда("пользователь нажимает кнопку Hide Password")
    public void пользовательНажимаетКнопкуHidePassword() {
        basicPage.ShowHide();
    }

    @Тогда("пароль заменяется астериксами")
    public void парольЗаменяетсяАстериксами() {
        basicPage.hidepass();
    }
}
