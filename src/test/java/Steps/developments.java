package Steps;

import org.junit.Assert;

import java.util.Timer;
import java.util.TimerTask;

import static java.lang.Thread.sleep;

public class developments {
    public void loader() throws InterruptedException {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("Запущен таймер");
                Assert.fail("Time Out");
            }
        }, 0, 3000);
        this.checkLoading();
    }
    public void checkLoading () throws InterruptedException {
        sleep(2000);
        if (page.isVisible(".Loading-Animation")) {
            checkLoading();
        }
    }
}
