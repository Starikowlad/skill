package Steps;

import io.cucumber.java.ru.Когда;
import io.cucumber.java.ru.Тогда;
import pages.BasicPage;

public class Continue {
    BasicPage basicPage = new BasicPage();
    @Когда("пользователь нажимает кнопку Continue")
    public void пользовательНажимаетКнопкуContinue() {
        basicPage.clickButton();
    }

}
