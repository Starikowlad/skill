package Steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.ru.*;
import org.junit.Assert;
import org.openqa.selenium.By;
import pages.BasicPage;

import java.util.ArrayList;

import static com.codeborne.selenide.Selenide.*;

public class Tittle {
    BasicPage basicPage = new BasicPage();

    @Тогда("отображается главная страница {string}")
    public void отображаетсяГлавнаяСтраница(String arg0) throws InterruptedException {
        basicPage.tittle(arg0);
    }


    @Тогда("появится сообщение {string}")
    public void появитсяСообщение(String arg0) {
        basicPage.tittle(arg0);
    }

    @Тогда("отображается экран выбора аккаунта Google")
    public void отображаетсяЭкранВыбораАккаунтаGoogle() {

    }

    @Тогда("отображается экран {string} Google")
    public void отображаетсяЭкранGoogle(String arg0) {
        basicPage.tittle(arg0);
    }

    @Тогда("отображается экран {string} в аккаунт Microsoft")
    public void отображаетсяЭкранВАккаунтMicrosoft(String arg0) {
        basicPage.tittle(arg0);
    }

    @Тогда("отображается главная страница Mpass")
    public void отображаетсяГлавнаяСтраницаMpass() {
        basicPage.Mpass();
    }

    @Тогда("отображается экран {string}")
    public void отображаетсяЭкран(String arg0) {
        basicPage.tittle(arg0);
    }

    @То("остаемся на этом же экране {string}")
    public void остаемсяНаЭтомЖеЭкране(String arg0) {
        basicPage.tittle(arg0);
    }

    @Тогда("отображается раздел {string}")
    public void отображаетсяРаздел(String arg0) {
        basicPage.tittle(arg0);
    }

    @Тогда("отображается экран {string} student,teacher,parents")
    public void отображаетсяЭкранStudentTeacherParents(String arg0) {
        basicPage.tittle(arg0);
    }

    @Тогда("язык приложения изменится на финскиий")
    public void языкПриложенияИзменитсяНаФинскиий() {
        $(By.xpath("//*[text()='Harjoittele']")).should(Condition.visible);
    }

    @Тогда("язык прииложения изменится на английский")
    public void языкПрииложенияИзменитсяНаАнглийский() {
        $(By.xpath("//*[text()='Focus']")).should(Condition.visible);
    }

    @Тогда("язык приложения изменится на узбекский")
    public void языкПриложенияИзменитсяНаУзбекский() {
        $(By.xpath("//*[text()='Fokus']")).should(Condition.visible);
    }

    @Тогда("изображение выделяется зеленой рамкой")
    public void изображениеВыделяетсяЗеленойРамкой() {
        Assert.assertEquals("Avatar_Avatar__T4hqy Avatar_Selected__2FGeR",
                $(By.cssSelector(".AvatarListModal_AvatarList__3sUKr>div:nth-child(23)")).attr("class"));
    }

    @Тогда("откроется главная страница с измененным аватаром")
    public void откроетсяГлавнаяСтраницаСИзмененнымАватаром() {
        String FirstImg = $(By.cssSelector(".Avatar.medium>img")).attr("src");
        System.out.println("FirstImg = " + FirstImg);
        sleep(5000);
        String SecondImg = $(By.cssSelector(".Avatar.medium>img")).attr("src");
        System.out.println("SecondImg = " + SecondImg);
        языкПрииложенияИзменитсяНаАнглийский();
        if (!FirstImg.equals(SecondImg)) {
            System.out.println("Ok");
        }
        else {
            System.out.println("Error");
        }
    }

    @Тогда("откроется главная страница с неизмененным аватаром")
    public void откроетсяГлавнаяСтраницаСНеизмененнымАватаром() {
        String FirstImg = $(By.cssSelector(".Avatar.medium>img")).attr("src");
        System.out.println("FirstImg = " + FirstImg);
        sleep(5000);
        String SecondImg = $(By.cssSelector(".Avatar.medium>img")).attr("src");
        System.out.println("SecondImg = " + SecondImg);
        языкПрииложенияИзменитсяНаАнглийский();
        if (FirstImg.equals(SecondImg)) {
            System.out.println("Ok");
        }
        else {
            System.out.println("Error");
        }
    }
    @Тогда("отображаются темы выбранного курса")
    public void отображаютсяТемыВыбранногоКурса() {
        $(By.xpath("//*[text()='1.lk Kevät - Geometria']")).text().equals("1.lk Kevät - Geometria");
    }
    @Тогда("отображаются задания выбранной темы")
    public void отображаютсяЗаданияВыбраннойТемы() {
        $(By.cssSelector(".Practice_TopicDetails__3CmOD")).waitUntil(Condition.exist, 5000);
    }



    @Тогда("отображаются курсы {string} класса")
    public void отображаютсяКурсыКласса(String arg0) {
        basicPage.grade(arg0);
    }

    @Тогда("отображается подраздел All")
    public void отображаетсяПодразделAll() {
        $(By.cssSelector(".sc-hKgILt.kFsFGq")).should(Condition.exist);
    }

    @Тогда("подраздел All не отображается")
    public void подразделAllНеОтображается() {
        sleep(500000);
        $(By.cssSelector(".sc-hKgILt.beOqPw")).should(Condition.exist);
    }



    @Тогда("отображается пустой экран или {string}")
    public void отображаетсяПустойЭкранИли(String arg0) {

        ElementsCollection cards = $$(".Leaderboard_LeaderboardRow__2a5B5");
        if (cards.size() > 0) {
            basicPage.tittle(arg0);
        }
        else {
            String empty = $(By.cssSelector(".Leaderboard_Text__3M_ya")).text();
            Assert.assertEquals("No one here yet.\n" +
                    "What are you waiting for?\n" +
                    "Start training and go to the top!", empty);
        }
    }

    @Тогда("происходит возврат на главный экран {string}")
    public void происходитВозвратНаГлавныйЭкран(String arg0) {
        basicPage.tittle(arg0);
    }
}


