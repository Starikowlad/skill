package Steps;


import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.By;
import pages.BasicPage;


import static com.codeborne.selenide.Selenide.*;

public class Hooks {
BasicPage basicPage = new BasicPage();
Click click = new Click();
    @Before(value = "@login")
    public void openUrl() {
        open();

        open("https://www.skillgrower.com/");

    }
    @Before(value = "@main_page")
    public void main_page() {
        open("https://www.skillgrower.com/");
        $(By.xpath("//*[text()='Login']")).should(Condition.exist).click();
        basicPage.inputEmail();
        basicPage.inputPassword();
        basicPage.clickButton();
        click.пользовательВыбираетАнглийскийЯзык();
    }
    @Before(value = "@homework")
    public void homework() {
        open("https://www.skillgrower.com/");
        $(By.xpath("//*[text()='Login']")).should(Condition.exist).click();
        basicPage.inputEmail();
        basicPage.inputPassword();
        basicPage.clickButton();
        click.пользовательВыбираетАнглийскийЯзык();
        $(By.xpath("//*[text()='Homework']")).click();
    }
    @Before(value = "@leaderboards")
    public void leaderboards() {
        open("https://www.skillgrower.com/");
        $(By.xpath("//*[text()='Login']")).should(Condition.exist).click();
        basicPage.inputEmail();
        basicPage.inputPassword();
        basicPage.clickButton();
        click.пользовательВыбираетАнглийскийЯзык();
        $(By.xpath("//*[text()='Leaderboards']")).click();
    }
    @Before(value = "@join_group")
    public void joingroup() {
        open("https://www.skillgrower.com/");
        $(By.xpath("//*[text()='Login']")).should(Condition.exist).click();
        basicPage.inputEmail();
        basicPage.inputPassword();
        basicPage.clickButton();
        click.пользовательВыбираетАнглийскийЯзык();
        $(By.xpath("//*[text()='Join group']")).click();
    }

    @After
    public void close() {
        Selenide.clearBrowserCookies();
        Selenide.clearBrowserLocalStorage();
        closeWebDriver();
    }


       // executeJavaScript( "document.cookie = \"auth_token= ; expires = Thu, 01 Jan 1970 00:00:00 GMT\"");






}
