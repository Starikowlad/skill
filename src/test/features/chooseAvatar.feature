# language: ru
@main_page
Функция: выбор аватара
  Предыстория: браузер открыт на странице skillgrower.com. пользователь зарегистрирован и находится на главной странице
  Сценарий: пользователь выбирает аватар
    Когда пользователь нажимает на аватар
    Тогда отображается экран "Choose avatar"
    Когда пользователь выбирает аватар
    Тогда изображение выделяется зеленой рамкой
  @main_page
    Сценарий: пользователь скроллит страницу
      Когда пользователь нажимает на аватар
      Тогда отображается экран "Choose avatar"
      Когда пользователь скроллит страницу
      Тогда страница проскроллена
  @main_page
  Сценарий: замена аватара
    Когда пользователь нажимает на аватар
    Тогда отображается экран "Choose avatar"
    Когда пользователь выбирает аватар
    Тогда изображение выделяется зеленой рамкой
    Когда пользователь нажимает кнопку "Choose"
    Тогда откроется главная страница с измененным аватаром
  @main_page
  Сценарий: нажатие на кнопку Cancel
    Когда пользователь нажимает на аватар
    Тогда отображается экран "Choose avatar"
    Когда пользователь выбирает аватар
    Тогда изображение выделяется зеленой рамкой
    Когда пользователь нажимает кнопку "Cancel"
    Тогда откроется главная страница с неизмененным аватаром