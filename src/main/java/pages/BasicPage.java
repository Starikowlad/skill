package pages;


import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.junit.Assert;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class BasicPage {
    public void clickButton() {
      $(By.xpath("//button[@name='action']")).click();
    }
    public void click(String text) {
        $(By.xpath("//*[text()='" + text + "']")).click();
    }
    public void inputEmail() {
        $(By.xpath("//input[@id='username']")).sendKeys("sk1@tanninen.at");
    }
    public void inputPassword() {
        $(By.xpath("//input[@id='password']")).sendKeys("Hipski60");
    }
    public void ShowHide() {
        $(By.cssSelector(".ca8f0ad03")).click();
    }
    public void tittle(String text) {
        $(By.xpath("//*[text()='" + text + "']")).should(Condition.visible);
    }
    public void inputInvalidEmail() {
        $(By.xpath("//input[@id='username']")).sendKeys("ssssssk1@tanninen.at");
    }
    public void inputInvalidPass() {
        $(By.xpath("//input[@id='password']")).sendKeys("Hipski6");

    }
    public void showpass() {
        Assert.assertEquals("text",$(By.xpath("//input[@id='password']")).attr("type"));
    }
    public void hidepass() {
        Assert.assertEquals("password",$(By.xpath("//input[@id='password']")).attr("type"));
    }
    public void Mpass() {
        $(By.xpath("/html/body/header/div[1]/div[1]/img")).should(Condition.visible);
    }
    public void clickSpan(String text) {
        $(By.xpath("//*/span[text()='" + text +"']")).click();
    }
    public void grade(String arg0) {
        SelenideElement Course = $$(".H2-module_H2__3tgHt").get(5);
       String g1 = Character.toString(Course.text().charAt(0));
       if (arg0.equals(g1)) {
           System.out.println("Ok");
       }
       else {
           System.out.println("Error");
       }
    }


}

